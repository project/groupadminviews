
DESCRIPTION
-----------
This is an add on for the Group module, which enables administrators to view all
the group specific content as well as for users who only have access to one or
multiple groups to be able to edit the content specific for that group.


REQUIREMENTS
------------
- Group module (http://drupal.org/project/group).
- Views module (http://drupal.org/project/views).
- Group context module (https://www.drupal.org/sandbox/mike.davis/2568111).


USAGE
-----
- You will find a new menu item within the 'Content' menu for listing all the
  content related to groups
- When viewing a group there is a new tab called 'Content' which lists the
  content for that group.


CREDITS
-------
Authored and maintained by Mike Davis (mike.davis).
